class Demo{
	int x=10;
	private int y=20;

	void fun(){
		System.out.println(x);//10	
		System.out.println(y);//20
	}
}

class Client{
	public static void main(String[] args){
		Demo obj=new Demo();

		obj.fun();

		/*System.out.println(x); //we can't acces  variable of another class w/o using object
		System.out.println(y);//same*/

		System.out.println(obj.x);//accesssing by using obj
		//System.out.println(obj.y);// we can't access bcz it is private variable
				      
	}
}

