class Demo{
	int x=22;

	Demo(){
		System.out.println("In no arg c");
	}

	Demo(int x){
		this();
		System.out.println("In para c");
	}

	public static void main(String[] args){
		Demo obj=new Demo(50);
	}
}
