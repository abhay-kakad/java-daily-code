class Demo{

	int x=50;

	static int y=20;

	Demo(){
		System.out.println("In Constructor");
	}

	static{

		System.out.println("In static block1");
	}

	//instace block
	{


	
	  System.out.println("In Instance  block1");
	  System.out.println(x);
	}

	{

		System.out.println("In Instance  block2");
	}

	public static void main(String[] args){

		System.out.println("In Main ");

		Demo obj=new Demo();
	}

	static {

		System.out.println("In Static block2 ");
	}

	{

		System.out.println("In Instace block3 ");
	}
}

