class SDemo{

	int x=10;
	static int y=20;

	static {
		System.out.println("In static block");
	}

	public static void main(String[] args){
		SDemo obj=new SDemo();

		System.out.println(obj.x);
	}

	static {
		
		System.out.println("IN static block2");
		//System.out.println(x);\\error 
		System.out.println(y);
	}
}
