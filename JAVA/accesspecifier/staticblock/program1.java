//static block comes first in picture before than MAIN method
class Demo{
	static{
		System.out.println("In static block");
	}

	public static void main(String[] args){

		System.out.println("IN Main method");
	}

	static {
		System.out.println("IN static block2");
	}
}
