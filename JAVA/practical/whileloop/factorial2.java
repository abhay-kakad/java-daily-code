//factorial
// i/p=6
// o/p=720
//
import java.util.Scanner;
class factorial{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter no for factorial");
		int no=sc.nextInt();

		int fact=1;

		while(no>0){
			fact=fact*no;
			no--;
		}
		System.out.println(fact);
	}
}
