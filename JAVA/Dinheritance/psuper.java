class Parent{
	int x=10;
	int y=20;

	Parent(){
		System.out.println("In parent Const");
	}
}

class Child extends Parent{

	int x=100;
	int y=200;

	Child(){

		System.out.println("In child Const");
	}

	void access(){

		System.out.println(super.x);//super use for printting super class thing means parent
		System.out.println(super.y);
		System.out.println(this.x);//for self class thing
		System.out.println(y);
	}
}

class Client{
	public static void main(String[] args){
		Child obj=new Child();

		obj.access();
	}
}
