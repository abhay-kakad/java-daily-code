class Parent{
	int x=10;

	Parent(){
		System.out.println("IN Parent constructor");
	}

	void access(){

		System.out.println("IN Parent instance");
	}
}

class Child extends Parent{
	int y=20;
	Child(){

		System.out.println("IN child constructor");
		System.out.println(x);
	
		System.out.println(y);
	}
	}
class Client{
	public static void main(String[] args){
		Child obj=new Child();

		obj.access();
	}
}

