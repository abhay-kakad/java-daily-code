class Parent{
	Parent(){
		System.out.println("IN parent constructor");
	}

	void fun(){

		System.out.println("IN parent fun");
	}
}

class Child extends Parent{

	Child(){

		System.out.println("IN child Constructor");
	}

	void fun(){

		System.out.println("IN child fun");
	}
}

class Client{
	public static void main(String[] args){
		Child obj=new Child();

		obj.fun();
	}
}
