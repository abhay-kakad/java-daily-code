class Parent{
	int x=10;
	static int y=20;
	static{
		System.out.println("In parent Static B");
	}

	Parent(){

		System.out.println("In parent Const");
	}

	void method1(){

		System.out.println(x);
		System.out.println(y);
	}

	static void method2(){

		System.out.println(y);
		
	}
}


class Child extends Parent{
	static {

		System.out.println("In child Static B");
	}

	Child(){

		System.out.println("In child Const");
	}
}

class Client{
	public static void main(String[] args){

		Child obj=new Child();

		obj.method1();
		obj.method2();
	}
}
