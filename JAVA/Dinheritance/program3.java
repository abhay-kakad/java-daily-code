class Parent{
	
	static{
		System.out.println("IN P satic block");
	}
}

class Child extends Parent{
	static{

		System.out.println("IN C satic block");
	}
}

class Client{
	public static void main(String[] args){
		Child obj=new Child();
	}
}
